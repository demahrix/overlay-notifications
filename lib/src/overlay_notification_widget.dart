import 'package:flutter/material.dart';
import 'package:overlay_notifications/overlay_notifications.dart';

class OverlayNotificationWidget extends StatelessWidget {

  final Widget? icon;
  final Color color;
  final Widget title;
  final Widget? description;

  const OverlayNotificationWidget({
    required this.icon,
    required this.color,
    required this.title,
    this.description
  });

  const OverlayNotificationWidget.success({
    this.icon = const Icon(Icons.check_circle_rounded, color: Colors.green),
    this.color = Colors.green,
    required this.title,
    this.description
  });

  const OverlayNotificationWidget.error({
    this.icon = const Icon(Icons.error_rounded, color: Colors.red),
    this.color = Colors.red,
    required this.title,
    this.description
  });

  const OverlayNotificationWidget.warning({
    this.icon = const Icon(Icons.warning_rounded, color: const Color(0xFFF57F17)),
    this.color = const Color(0xFFF57F17),
    required this.title,
    this.description
  });

  const OverlayNotificationWidget.wrongThing(): 
    this.icon = const Icon(Icons.warning_rounded, color: Colors.red),
    this.color = Colors.red,
    this.title = const Text('Un problème rencontré'),
    this.description = const Text('Une erreur s\'est produite veuillez réesayer plus tard');

  const OverlayNotificationWidget.networkError(): 
    this.icon = const Icon(Icons.wifi_off_rounded, color: Colors.red),
    this.color = Colors.red,
    this.title = const Text('Un problème rencontré'),
    this.description = const Text('Veuillez vérifier votre connexion à internet et réessayer');

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context).colorScheme;

    return Container(
      decoration: BoxDecoration(
        boxShadow: kElevationToShadow[3],
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(4.0)),
        child: Container(
          width: 320.0,
          padding: const EdgeInsets.only(
            top: 16.0,
            // left: 0.0,
            right: 16.0,
            bottom: 8.0
          ),
          decoration: BoxDecoration(
            color: theme.primaryContainer,
            // borderRadius: const BorderRadius.all(Radius.circular(4.0)),
            // boxShadow: kElevationToShadow[3],
            border: Border(
              left: BorderSide(
                width: 4.0,
                color: color
              )
            )
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: icon,
              ),

              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    DefaultTextStyle(
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: theme.onPrimaryContainer
                      ),
                      child: title
                    ),

                    if (description != null)
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: DefaultTextStyle(
                          style: TextStyle(
                            color: theme.onSecondaryContainer
                          ),
                          child: description!
                        ),
                      ),

                    const SizedBox(height: 8.0),

                    Align(
                      alignment: Alignment.topRight,
                      child: TextButton(
                        onPressed: OverlayNotifications.instance.close,
                        child: const Text('Fermer')
                      ),
                    )
                  ],
                ),
              ),
      
            ],
          ),
        ),
      ),
    );
  }
}
