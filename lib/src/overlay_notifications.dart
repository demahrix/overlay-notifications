import 'dart:async' show Timer;
import 'package:flutter/material.dart';
import 'package:overlay_notifications/overlay_notifications.dart';

class OverlayNotifications {

  OverlayNotifications._();

  static OverlayNotifications? _instance;
  OverlayEntry? _currentNotification;
  Timer? _timer;

  static OverlayNotifications get instance {
    if (_instance == null)
      _instance = OverlayNotifications._();
    return _instance!;
  }

  void notify({
    required BuildContext context,
    double? top,
    double? left,
    double? right = 40.0,
    double? bottom = 70.0,
    required OverlayNotificationWidget notification,
    Duration duration = const Duration(seconds: 4)
  }) {
      // S'il y a un overlay il le ferme avant afficher le overlay actuelle
      close();
      _currentNotification = OverlayEntry(builder: (_) => Positioned(
        top: top,
        left: left,
        right: right,
        bottom: bottom,
        child: notification,
      ));
      Overlay.of(context).insert(_currentNotification!);

      _timer = Timer(duration, close) ;
  }

  void close() {
    if (_currentNotification != null) {
      _currentNotification!.remove();
      _currentNotification = null;
      if (_timer?.isActive == true) {
        _timer?.cancel();
      }
    }
  }

}
